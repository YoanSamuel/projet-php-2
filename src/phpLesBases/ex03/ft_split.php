<?php

function ft_split(string $var): array
{
    $array = preg_split("/[\s,]+/", $var, -1, PREG_SPLIT_NO_EMPTY);
    sort($array);

    return $array;
}

// print_r (ft_split(fgets(STDIN)));
