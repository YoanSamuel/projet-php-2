<?php

function ft_is_sort($tab)
{
    $default = $tab;
    sort($tab);

    return $default === $tab;
}
