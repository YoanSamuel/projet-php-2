<?php

if ($argc > 1) {
    // Créer une Array
    $arr = preg_split("/\s/", $argv[1]);

    // Récupérer le premier élément array_shift et Supprimer le premier élément du tableaux arr1
    // $firstElement = $arr[0];
    // $arr = array_slice($arr, 1);

    $firstElement = array_shift($arr);

    // Ajouter le tableaux arr2 a la suite de arr1
    $arr[] = "$firstElement";
    // array_push($arr, "$firstElement");

    // Convert in String
    $output = implode(' ', $arr);
    $output = preg_replace('/\s+/', ' ', $output);
    echo trim($output) . "\n";
}
